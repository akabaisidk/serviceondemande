package gn.kaba;

import java.util.Scanner;

public class Main {

    /*psvm*/
    public static void main(String[] args) {

        /*sout*/
        System.out.println("Bienvenue sur la plateforme service a la demande !");
        System.out.println("> l pour lister les services disponibles ");
        //La classe Scanner pour lire en ligne de commande
        Scanner scanner = new Scanner(System.in);
        String menu = scanner.next();

        //Si vous appuyer sur l vous aurez la liste des services
        if("l".equalsIgnoreCase(menu)){
            //Afficher la liste des services
            System.out.println("Les services disponibles sont :");
            System.out.println("> 1 Coursier");
            System.out.println("> 2 Nettoyage");
            System.out.println("> 3 Tutorat");

            int idPrestation = scanner.nextInt();
            if (1 == idPrestation){
                //Afficher les prestataires qui fournissent ce service
                System.out.println("Liste des Prestataires coursier: ");
                System.out.println("> 1 Habib Sow");
                System.out.println("> 2 Moussa Cisse");
                System.out.println("> 3 Mouctar Traore");

                int idPrestataire = scanner.nextInt();
                if(2 == idPrestataire){

                    //Afficher les prestations d'un prestataire
                    System.out.println("Les details des prestations de Moussa Cisse: ");
                    System.out.println("> 1 Course Dixinn : 5000");
                    System.out.println("> 2 Course Matam : 10000");
                    System.out.println("> 3 Course Ratoma : 15000");

                    int idDetailsPrestation = scanner.nextInt();
                    if(3 == idDetailsPrestation){

                        System.out.println("Prestation  Moussa Cisse : Course Ratoma 15000 ");
                        System.out.println("Quelle heure voulez-vous (hh:mm) ? ");

                        String heure = scanner.next();
                        //F
                        System.out.println(String.format("Ok pour %s",heure));
                    }

                }
            }
        }

        else {
            System.out.println("Choix inconnu !");
        }

    }
}
